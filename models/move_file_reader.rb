require_relative "move_parser"

class MoveFileReader
  def initialize(file)
    @file = file
  end

  def moves
    File.open(@file).each_char.lazy.map do |move_command|
      MoveParser.parse(move_command.to_i)
    end
  end
end
