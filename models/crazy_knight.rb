require "set"
require_relative "knight"
require_relative "euclidean_distance"

class CrazyKnight
  attr_reader :moves_reader,
              :knight,
              :last_position,
              :furthest_position,
              :same_x_y_positions

  def initialize(moves_reader:, knight: Knight.new)
    @moves_reader = moves_reader
    @knight = knight
  end

  def walk
    same_x_y_positions = Set.new
    furthest_position = nil
    furthest_distance = 0

    @moves_reader.moves.each do |move|
      @knight.move(move)

      same_x_y_positions << @knight.position if @knight.x == @knight.y

      position_distance = EuclideanDistance.calculate(x1: 0, y1: 0, x2: @knight.x, y2: @knight.y)

      if position_distance > furthest_distance
        furthest_position = @knight.position
        furthest_distance = position_distance
      end
    end

    # this is in case the knight returns to its initial position
    same_x_y_positions -= [[0, 0]]

    @last_position = @knight.position
    @furthest_position = furthest_position
    @same_x_y_positions = same_x_y_positions.to_a
  end
end
