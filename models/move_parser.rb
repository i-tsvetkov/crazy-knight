require_relative "move"

class MoveParser
  MOVES_MAPPING = {
    1 => Move.new(x_offset: 1, y_offset: 2),
    2 => Move.new(x_offset: 2, y_offset: 1),
    3 => Move.new(x_offset: 2, y_offset: -1),
    4 => Move.new(x_offset: 1, y_offset: -2),
    5 => Move.new(x_offset: -1, y_offset: -2),
    6 => Move.new(x_offset: -2, y_offset: -1),
    7 => Move.new(x_offset: -2, y_offset: 1),
    8 => Move.new(x_offset: -1, y_offset: 2),
  }

  def self.parse(move_number)
    MOVES_MAPPING[move_number] or raise "Move command #{move_number} not recognized"
  end
end
