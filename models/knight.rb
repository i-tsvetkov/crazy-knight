class Knight
  attr_reader :x, :y

  def initialize(x: 0, y: 0)
    @x, @y = x, y
  end

  def move(move)
    @x += move.x_offset
    @y += move.y_offset
  end

  def position
    [@x, @y]
  end
end
