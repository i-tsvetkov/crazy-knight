class EuclideanDistance
  def self.calculate(x1:, y1:, x2:, y2:)
    Math.sqrt((x1 - x2)**2 + (y1 - y2)**2)
  end
end
