#!/usr/bin/env ruby
require_relative "models/crazy_knight"
require_relative "models/move_file_reader"

crazy_knight = CrazyKnight.new(moves_reader: MoveFileReader.new("moves"))
crazy_knight.walk

puts "What is the position of the knight figure after executing all the moves in the attached file?"
puts crazy_knight.last_position.inspect

puts "What is the furthest point on the board from the initial position (0, 0) that the figure has ever been while executing the list of instructions?"
puts crazy_knight.furthest_position.inspect

puts "Are there any positions where x=y, excluding the initial position (0, 0)? If yes, which ones?"
if crazy_knight.same_x_y_positions.any?
  puts "Yes!"
  puts crazy_knight.same_x_y_positions.inspect
else
  puts "No"
end
