require "tempfile"
require_relative "../../models/move_file_reader"

describe MoveFileReader do

  it "reads correctly moves from a file" do
    file = Tempfile.new
    file.write("123321")
    file.close

    reader = MoveFileReader.new(file.path)

    expect(reader.moves.to_a).to eq([
      Move.new(x_offset: 1, y_offset: 2),
      Move.new(x_offset: 2, y_offset: 1),
      Move.new(x_offset: 2, y_offset: -1),
      Move.new(x_offset: 2, y_offset: -1),
      Move.new(x_offset: 2, y_offset: 1),
      Move.new(x_offset: 1, y_offset: 2),
    ])
  ensure
    file.unlink
  end

end
