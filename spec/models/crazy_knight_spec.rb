require_relative "../../models/crazy_knight"
require_relative "../../models/move_file_reader"

describe CrazyKnight do
  it "finds the last position of the knight" do
    file = Tempfile.new
    file.write("1865775")
    file.close

    crazy_knight = CrazyKnight.new(moves_reader: MoveFileReader.new(file.path))
    crazy_knight.walk

    expect(crazy_knight.last_position).to eq([-8, 1])
  ensure
    file.unlink
  end

  it "finds the furthest position of the knight" do
    file = Tempfile.new
    file.write("186577")
    file.close

    crazy_knight = CrazyKnight.new(moves_reader: MoveFileReader.new(file.path))
    crazy_knight.walk

    expect(crazy_knight.furthest_position).to eq([-7, 3])
  ensure
    file.unlink
  end

  it "finds all of the positions with the same x and y coordinates" do
    file = Tempfile.new
    file.write("383838")
    file.close

    crazy_knight = CrazyKnight.new(moves_reader: MoveFileReader.new(file.path))
    crazy_knight.walk

    expect(crazy_knight.same_x_y_positions).to match_array([[1, 1], [2, 2], [3, 3]])
  ensure
    file.unlink
  end

  it "finds all of the positions with the same x and y coordinates except [0, 0]" do
    file = Tempfile.new
    file.write("4783383838")
    file.close

    crazy_knight = CrazyKnight.new(moves_reader: MoveFileReader.new(file.path))
    crazy_knight.walk

    expect(crazy_knight.same_x_y_positions).to match_array([[-1, -1], [1, 1], [2, 2], [3, 3]])
  ensure
    file.unlink
  end
end
