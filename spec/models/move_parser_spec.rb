require_relative "../../models/move_parser"

describe MoveParser do
  it "parse correctly given move number" do
    expect(MoveParser.parse(3)).to eq(Move.new(x_offset: 2, y_offset: -1))
  end

  it "raises an exception if the move number is invalid" do
    expect{ MoveParser.parse(13) }.to raise_error("Move command 13 not recognized")
  end
end
