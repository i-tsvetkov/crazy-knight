require_relative "../../models/knight"
require_relative "../../models/move"

describe Knight do

  it "has the right x and y coordinates when instantiated" do
    x, y = 42, 24

    knight = Knight.new(x: x, y: y)

    expect(knight.x).to eq(x)
    expect(knight.y).to eq(y)
  end

  it "moves in the right x and y directions per the given move" do
    move = Move.new(x_offset: 101, y_offset: 99)
    knight = Knight.new(x: 42, y: 24)

    knight.move(move)

    expect(knight.x).to eq(143)
    expect(knight.y).to eq(123)
  end

  it "returns the right position based on its coordinates" do
    x, y = 42, 24

    knight = Knight.new(x: x, y: y)

    expect(knight.position).to eq([x, y])
  end
end
