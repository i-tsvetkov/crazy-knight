require_relative "../../models/euclidean_distance"

describe EuclideanDistance do
  it "calculates correctly the euclidean distance" do
    expect(EuclideanDistance.calculate(x1: 0, y1: 0, x2: 1, y2: 1)).to be_within(Float::EPSILON).of(Math.sqrt(2))
    expect(EuclideanDistance.calculate(x1: 0, y1: 0, x2: 1_000_000, y2: 1_000_000)).to be_within(Float::EPSILON).of(Math.sqrt(1_000_000**2 + 1_000_000**2))
  end
end
